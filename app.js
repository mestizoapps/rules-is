//Librerys
var http 		 =require('http');
var soap 		 =require('soap');
var https   	 =require('https');
var express 	 =require('express');
var Promise 	 =require('promise');
var bodyParser 	 =require('body-parser');
var js2xmlparser =require("js2xmlparser");
var session 	 =require('express-session');
var XMLWriter    =require('xml-writer');

//Config
var app     = express();
var xw      = new XMLWriter;
var router  = express.Router();

var ip      = 'http://192.168.1.93';
var port    = process.env.PORT || 3000;
var url     = ip+':'+port+'/policy';
var credential = new soap.BasicAuthSecurity('admin', 'qwerty');
var wsdl = 'https://192.168.1.249:9443/services/EntitlementPolicyAdminService?wsdl';

router.use(bodyParser.json()); // support json encoded bodies
router.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//Controller
router.use(function(req, res, next){
    console.log(req.method, req.url);
    sess = req.session;
    next();
});

router.get('/listPolicy',function(req, res){
	var data = req.body;
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
	var options = {
		ignoredNamespaces: true
	}
    var getPolicy = {
    		searchString:'*'
    };

    soap.createClient(wsdl, options, function(err, client) {
		client.setSecurity(credential);
      	client.getAllPolicyIds(js2xmlparser.parse("getAllPolicyIds",getPolicy), function(err, result, raw) {
	      	console.log(result);
	      	res.send(result);
	    });
    });
});

/*router.post('/publish',function(req, res){
	var data = req.body;
	var wsdl = 'https://192.168.1.249:9443/services/EntitlementPolicyAdminService?wsdl';
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
	var options = {
		ignoredNamespaces: true
	}
    var message = {
    	policyIds: data.id,
    	subscriberIds: 'PDP Subscriber',
    	action:'CREATE',
    	version:1,
    	enabled:true,
    	order:0
    };

    //res.send(policy);

    
    soap.createClient(wsdl, options, function(err, client) {
		client.setSecurity(credential);
      	client.publishPolicies(message, function(err, result, raw) {
	      	console.log(result);
	      	res.send(err);
	    });
    });
});*/

router.post('/publishToPDP',function(req, res){
    var data = req.body;
    
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    var options = {
        ignoredNamespaces: true
    }
    var message = {
        policyIds: data.id,
        action:data.action,
        version:1,
        enabled:false,
        order:0
    };

    
    soap.createClient(wsdl, options, function(err, client) {
        client.setSecurity(credential);
        client.publishToPDP(message, function(err, result, raw) {
            console.log(result);
            res.send(result);
        });
    });
});

router.post('/enablePolicy',function(req, res){
    var data = req.body;
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    var options = {
        ignoredNamespaces: true
    }
    var message = {
        policyId: data.id,
        enable: data.enable,
    };

    
    soap.createClient(wsdl, options, function(err, client) {
        client.setSecurity(credential);
        client.enableDisablePolicy(message, function(err, result, raw) {
            console.log(result);
            res.send(result);
        });
    });
});

router.post('/makePolicy',function(req, res){
    //res.writeHead(200,{"content-type":"application/xml"});
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    
    var data = req.body;
    var options = {
        ignoredNamespaces: true
    }

    function match(param, type){
        xw.startElement('AnyOf').startElement('AllOf');
        xw.startElement('Match').writeAttribute('MatchId','urn:oasis:names:tc:xacml:1.0:function:string-equal');
        if(type == 'resource'){
            xw.startElement('AttributeValue').writeAttribute('DataType','http://www.w3.org/2001/XMLSchema#string').text(param).endElement();
            xw.startElement('AttributeDesignator').writeAttribute('AttributeId','urn:oasis:names:tc:xacml:1.0:resource:resource-id').writeAttribute('Category','urn:oasis:names:tc:xacml:3.0:attribute-category:resource').writeAttribute('DataType','http://www.w3.org/2001/XMLSchema#string').writeAttribute('MustBePresent','true').text('').endElement();
        }
        if(type == 'action'){
            xw.startElement('AttributeValue').writeAttribute('DataType','http://www.w3.org/2001/XMLSchema#string').text(param).endElement();
            xw.startElement('AttributeDesignator').writeAttribute('AttributeId','urn:oasis:names:tc:xacml:1.0:resource:resource-id').writeAttribute('Category','urn:oasis:names:tc:xacml:3.0:attribute-category:action').writeAttribute('DataType','http://www.w3.org/2001/XMLSchema#string').writeAttribute('MustBePresent','true').text('').endElement();
        }
        xw.endElement().endElement().endElement();//AllOf, AnyOf
        return xw;
    }

    function makeRule(data, action){
        xw.startElement("Rule").writeAttribute('Effect','Permit').writeAttribute('RuleId',action+data.policyName);
            xw.startElement('Target');
            match(data.resource,'resource');
            match(action,'action');
            xw.endElement();//Target
            xw.startElement('Condition');
            xw.startElement('Apply').writeAttribute('FunctionId','urn:oasis:names:tc:xacml:1.0:function:string-at-least-one-member-of');
            xw.startElement('Apply').writeAttribute('FunctionId','urn:oasis:names:tc:xacml:1.0:function:string-bag');
            if(action=='GET'){
                data.get.forEach(function(element){
                    xw.startElement('AttributeValue').writeAttribute('DataType','http://www.w3.org/2001/XMLSchema#string').text(element).endElement();                
                });
            } 
            if(action=='POST'){
                data.post.forEach(function(element){
                    xw.startElement('AttributeValue').writeAttribute('DataType','http://www.w3.org/2001/XMLSchema#string').text(element).endElement();                
                });
            }   
            if(action=='PUT'){
                data.put.forEach(function(element){
                    xw.startElement('AttributeValue').writeAttribute('DataType','http://www.w3.org/2001/XMLSchema#string').text(element).endElement();                
                });
            } 
            if(action=='DELETE'){
                data.delete.forEach(function(element){
                    xw.startElement('AttributeValue').writeAttribute('DataType','http://www.w3.org/2001/XMLSchema#string').text(element).endElement();                
                });
            } 
            xw.endElement();//Apply
            xw.startElement('AttributeDesignator').writeAttribute('AttributeId','http://wso2.org/claims/role').writeAttribute('Category','urn:oasis:names:tc:xacml:1.0:subject-category:access-subject').writeAttribute('DataType','http://www.w3.org/2001/XMLSchema#string').writeAttribute('MustBePresent','true').text('').endElement();
            xw.endElement().endElement();//Condition, Apply
            xw.endElement();//Rule
    }
   
    function start_policy(data){
        
        xw.startDocument();
        
        xw.startElement('Policy').writeAttribute('xmlns', 'urn:oasis:names:tc:xacml:3.0:core:schema:wd-17').writeAttribute('PolicyId', data.policyName).writeAttribute('RuleCombiningAlgId', 'urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:first-applicable').writeAttribute('Version', '1.0');    
        //Primer bloque target de la politica
        xw.startElement('Target').startElement('AnyOf').startElement('AllOf');
        xw.startElement('Match').writeAttribute('MatchId', 'urn:oasis:names:tc:xacml:1.0:function:string-regexp-match');
        xw.startElement('AttributeValue').writeAttribute('DataType', 'http://www.w3.org/2001/XMLSchema#string').text(data.resource).endElement();
        xw.startElement('AttributeDesignator').writeAttribute('AttributeId','urn:oasis:names:tc:xacml:1.0:resource:resource-id').writeAttribute('Category','urn:oasis:names:tc:xacml:3.0:attribute-category:resource').writeAttribute('DataType','http://www.w3.org/2001/XMLSchema#string').writeAttribute('MustBePresent','true').text('').endElement();
        xw.endElement().endElement().endElement().endElement();//Match, AllOf, AnyOf, Target
        
        if(data.get){
            makeRule(data, 'GET');
        }
        if(data.post){
            makeRule(data, 'POST');
        }
        if(data.put){
            makeRule(data, 'PUT');
        }
        if(data.delete){
            makeRule(data, 'DELETE');
        }
        xw.startElement("Rule").writeAttribute('Effect','Deny').writeAttribute('RuleId','deny-rule').text('').endElement();


        xw.endElement(); //Policy
        xw.endDocument();//Document
        
        return xw.toString();
    }

    var policy = start_policy(data);
    console.log(policy);
    //res.end(policy);
    var message = {
        policyDTO:{
            policy:policy
        }
    };

    //res.send(policy);
    
    soap.createClient(wsdl, options, function(err, client) {
        client.setSecurity(credential);
        client.addPolicy(message, function(err, result, raw) {
            console.log(result);
            res.send(result);
        });
    });
});

//Server On
app.use('/policy', router);



//Starter Server
app.listen(port);
console.log(url);
